# Rampl

This package contains functions to transform optimization problems
defined in **R** into [AMPL](https://ampl.com/) code.

The code in the package is based on code from the **fPortfolio** package.